#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import time
from ctypes import cdll

lib = cdll.LoadLibrary("./target/release/libsteroids.dylib")

start_time = time.time()
lib.parse("./data/BTC-USD.csv")
print("--- %s seconds ---" % (time.time() - start_time))

print("сделано!")