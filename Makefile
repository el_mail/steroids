ifeq ($(shell uname),Darwin)
    EXT := dylib
else
    EXT := so
endif

all: target/release/libsteroids.$(EXT)
	python python/the_best_bitcoin_parser.py

target/release/libsteroids.$(EXT): src/lib.rs Cargo.toml
	cargo build --release

clean:
	rm -rf target